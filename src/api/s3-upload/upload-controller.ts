import * as Hapi from '@hapi/hapi';
import { AWSService } from '../../common/aws/aws-services';

export default class UploadController {
  constructor(public readonly awsService: AWSService) {}

  //Upload a file to S3
  public upload = async (request, toolkit: Hapi.ResponseToolkit) => {
    await this.awsService
      .uploadFile(request.payload.file, request.payload.filename)
      .then(response => {
        return response;
      })
      .catch(err => {
        return err.message;
      });
  };

  //list all the files from S3
  public list = async () => {
    await this.awsService
      .listFiles()
      .then(response => {
        return response;
      })
      .catch(err => {
        return err.message;
      });
  };

  //Delete a file from S3
  public delete = async (request, toolkit: Hapi.ResponseToolkit) => {
    await this.awsService
      .deleteFile(request.payload.file)
      .then(response => {
        return response;
      })
      .catch(err => {
        return err.message;
      });
  };

  //Get a file from S3
  public get = async (request, toolkit: Hapi.ResponseToolkit) => {
    let file = encodeURIComponent(request.params.file);
    await this.awsService
      .getFile(file)
      .then(response => {
        return response;
      })
      .catch(err => {
        return err.message;
      });
  };
}
