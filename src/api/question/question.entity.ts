import { Entity, Column, PrimaryGeneratedColumn } from "typeorm";
import { BaseEntity } from "../../common/base.entity";

@Entity({
  name: 'question'
})

export class Question extends BaseEntity {

  @Column({ name: 'text', length: 255, nullable: false })
  text: string;

}
