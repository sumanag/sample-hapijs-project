import { Question } from './question.entity';
import * as Boom from '@hapi/boom';
import * as Hapi from '@hapi/hapi';
import newResponse from '../../helper/response';
import { getManager } from 'typeorm';
import logger from '../../helper/logger'

export default class QuestionController {
	public async getAll(request: Hapi.Request, toolkit: Hapi.ResponseToolkit): Promise<any> {
		try {
			let questionData = await getManager().getRepository(Question).find();
			if (questionData) {
				return toolkit.response(newResponse(request, { value: questionData }));
			}
		} catch (error) {
			logger.error(error.message);
			return toolkit.response(newResponse(request, { boom: Boom.badImplementation(error) }));
		}
	};

	public async getById(request: Hapi.Request, toolkit: Hapi.ResponseToolkit): Promise<any> {
		try {
			const id = encodeURIComponent(request.params.id);
			let questionData = await getManager().getRepository(Question).findOne(id);
			if (questionData) {
				return toolkit.response(newResponse(request, { value: questionData }));
			}
		} catch (error) {
			logger.error(error.message);
			return toolkit.response(newResponse(request, { boom: Boom.badImplementation(error) }));
		}
	};

	public async create(request: Hapi.Request, toolkit: Hapi.ResponseToolkit): Promise<any> {
		try {
			let repo = getManager().getRepository(Question);
			let data = await repo.save(request.payload as any)
			return toolkit.response(newResponse(request, { value: data }));
		} catch (error) {
			logger.error(error.message);
			return toolkit.response(newResponse(request, { boom: Boom.badImplementation(error) }));
		}
	};

	public async update(request: Hapi.Request, toolkit: Hapi.ResponseToolkit): Promise<any> {
		try {
			const id = encodeURIComponent(request.params.id);
			let updatedEntity = await getManager().getRepository(Question).update(id, request.payload as any);
			if (!updatedEntity) {
				return toolkit.response(newResponse(request, { boom: Boom.notFound() }));
			}
			return toolkit.response(newResponse(request, { value: updatedEntity }));
		} catch (error) {
			logger.error(error.message);
			return toolkit.response(newResponse(request, { boom: Boom.badImplementation(error) }));
		}
	};

	public async delete(request: Hapi.Request, toolkit: Hapi.ResponseToolkit): Promise<any> {
		try {
			const id = encodeURIComponent(request.params.id);
			getManager().getRepository(Question).delete(id);
			return toolkit.response(newResponse(request, { value: { id: id } }));
		} catch (error) {
			logger.error(error.message);
			return toolkit.response(newResponse(request, { boom: Boom.badImplementation(error) }));
		}
	};
}
