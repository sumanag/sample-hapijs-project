import * as Hapi from '@hapi/hapi';
import QuestionController from './question-controller';
import logger from '../../helper/logger';
import IRoute from '../../helper/route';
import validate from './question-validate';

export default class QuestionRoutes implements IRoute {
  public async register(server: Hapi.Server): Promise<any> {
    return new Promise(resolve => {
      logger.info('Started - Question routes');
      const controller = new QuestionController();
      server.route([
        {
          method: 'GET',
          path: '/api/questions',
          options: {
            handler: controller.getAll,
            description: 'Method that gets all questions.',
            auth: false,
          },
        },
        {
          method: 'GET',
          path: '/api/questions/{id}',
          options: {
            handler: controller.getById,
            validate: validate.getById,
            description: 'Method to get a question by its id.',
            auth: false,
          },
        },
        {
          method: 'POST',
          path: '/api/questions',
          options: {
            handler: controller.create,
            validate: validate.create,
            description: 'Method to create a new question.',
            auth: false,
          },
        },
        {
          method: 'PUT',
          path: '/api/questions/{id}',
          options: {
            handler: controller.update,
            validate: validate.updateById,
            description: 'Method to update a question by its id.',
            auth: false,
          },
        },
        {
          method: 'DELETE',
          path: '/api/questions/{id}',
          options: {
            handler: controller.delete,
            validate: validate.deleteById,
            description: 'Method that deletes a user by its id.',
            auth: false,
          },
        },
      ]);
      logger.info('Completed - Question routes');
      resolve();
    });
  }
}