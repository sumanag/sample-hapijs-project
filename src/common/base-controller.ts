import { BaseEntity } from './base.entity';
import * as Boom from '@hapi/boom';
import * as Hapi from '@hapi/hapi';
import logger from '../helper/logger';
import newResponse from '../helper/response';
import { getManager } from 'typeorm';
import BaseService from './base-service';

let baseService = new BaseService();

export default class BaseController<T> {
  constructor(public id?: string) {
  }

  //GET all the entries
  public async getAll(request: Hapi.Request, toolkit: Hapi.ResponseToolkit): Promise<any> {
    try {
      logger.info(`GET - ${request.url.href}`);
      let result = baseService.getAll;
      if (result) {
        return toolkit.response(newResponse(request, { value: result }));
      }
    } catch (error) {
      return toolkit.response(newResponse(request, { boom: Boom.badImplementation(error) }));
    }
  };

  //GET by id
  public async getById(request: Hapi.Request, toolkit: Hapi.ResponseToolkit): Promise<any> {
    try {
      logger.info(`GET - ${request.url.href}`);
      const id = encodeURIComponent(request.params[this.id]);
      let result = await getManager().getRepository(BaseEntity).findOne(id);
      if (result) {
        return toolkit.response(newResponse(request, { value: result }));
      }
    } catch (error) {
      return toolkit.response(newResponse(request, { boom: Boom.badImplementation(error) }));
    }
  };

  //POST an entry
  public async create(request: Hapi.Request, toolkit: Hapi.ResponseToolkit): Promise<any> {
    try {
      logger.info(`POST - ${request.url.href}`);
      let repo = getManager().getRepository(BaseEntity);
      let data = await repo.save(request.payload as any)
      return toolkit.response(newResponse(request, { value: data }));
    } catch (error) {
      return toolkit.response(newResponse(request, { boom: Boom.badImplementation(error) }));
    }
  };

  //Update an entry 
  public async update(request: Hapi.Request, toolkit: Hapi.ResponseToolkit): Promise<any> {
    try {
      logger.info(`PUT - ${request.url.href}`);
      const id = encodeURIComponent(request.params[this.id]);
      let updatedEntity = getManager().getRepository(BaseEntity).update(id, request.payload as any);
      if (!updatedEntity) {
        return toolkit.response(newResponse(request, { boom: Boom.notFound() }));
      }
      return toolkit.response(newResponse(request, { value: updatedEntity }));
    } catch (error) {
      return toolkit.response(newResponse(request, { boom: Boom.badImplementation(error) }));
    }
  };

  //Delete an entry
  public async delete(request: Hapi.Request, toolkit: Hapi.ResponseToolkit): Promise<any> {
    try {
      logger.info(`DELETE - ${request.url.href}`);
      const id = encodeURIComponent(request.params[this.id]);
      getManager().getRepository(BaseEntity).delete(id);
      return toolkit.response(newResponse(request, { value: { id: id } }));
    } catch (error) {
      return toolkit.response(newResponse(request, { boom: Boom.badImplementation(error) }));
    }
  };
}
